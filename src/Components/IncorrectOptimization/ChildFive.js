import React from "react";

const ChildFive = ({ name }) => {
  console.log("child five rendering");
  return <div>Hello {name}</div>;
};

export const MemoizedChildFive = React.memo(ChildFive);

import React, { useState } from "react";
// import { MemoizedChildThree } from "./ChildThree";
import { MemoizedChildFour } from "./ChildFour";

const ParentThree = () => {
  const [count, setCount] = useState(0);
  const [name, setName] = useState("Sweta");
  console.log("ParentThree rendering");
  return (
    <div>
      <button onClick={() => setCount((c) => c + 1)}>Count -{count}</button>
      <button onClick={() => setName("Sweta Dhakal")}>ChangeName</button>
      {/* <MemoizedChildThree name={name}>
        <strong>Hello</strong>
      </MemoizedChildThree> */}

      <MemoizedChildFour name={name}></MemoizedChildFour>
    </div>
  );
};

export default ParentThree;

import React, { useState, useMemo, useCallback } from "react";
import { MemoizedChildFive } from "./ChildFive";

const ParentFour = () => {
  const [count, setCount] = useState(0);
  const [name, setName] = useState("Sweta");

  const person = {
    fname: "switu",
    lname: "dhakall",
  };

  const memoizedPerson = useMemo(() => person, []);

  const handleClick = () => {};

  const memoizedHandleClick = useCallback(handleClick, []);

  console.log("ParentFour rendering");
  return (
    <div>
      <button onClick={() => setCount((c) => c + 1)}>Count -{count}</button>
      <button onClick={() => setName("Sweta Dhakal")}>ChangeName</button>
      {/* <MemoizedChildFive
        name={name}
        person={memoizedPerson}
      ></MemoizedChildFive> */}
      <MemoizedChildFive
        name={name}
        handleClick={memoizedHandleClick}
      ></MemoizedChildFive>
    </div>
  );
};

export default ParentFour;

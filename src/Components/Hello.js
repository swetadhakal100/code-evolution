import React from "react";

const Hello = () => {
    //this is jsx example
    // return (
    //     <div className= 'dummyClass'>
    //         <h1>Hello Sweta</h1>
    //     </div>
    // )
    //as we know browser doesnot understand jsx so react convert jsx into pure javascript  like this in below example while running react project so to convert jsx into javascript we need to import react from react library.
    //react is a export default module and component is a export named class of that react so while importing curly braces is used.
    return React.createElement('div', { id: 'hello', className: 'dummyClass' }, React.createElement('h1', null, 'Hello Sweta'))

}
export default Hello
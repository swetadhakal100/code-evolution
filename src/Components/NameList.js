import React from 'react'
import Person from './Person'

function NameList() {
    //first and second approach
    // const names = ['Bruce', 'Clark', 'Diana']
    //third approach
    // const NameList =  names.map(name => <h2>{name}</h2>)
    //last approach
    // const persons = [
    //     {
    //         id: 1,
    //         name: 'Bruce',
    //         age: 30,
    //         skill: 'React'
    //     },
    //     {
    //         id: 2,
    //         name: 'Clark',
    //         age: 25,
    //         skill: 'Angular'
    //     },
    //     {
    //         id: 3,
    //         name: 'Diana',
    //         age: 28,
    //         skill: 'Vue'
    //     }


    // ]
    // const personList = persons.map(person => 
    //     <Person key={person.name} person={person} />
    // )


    //Index as key Anti-Pattern
    const names = ['Bruce', 'Clark', 'Diana', 'Bruce']
     const NameList = names.map((name, index) => 
        <h2 key={index}>{index } {name}</h2>
    )


    return (
        <div>
            <div>{NameList}</div>
            {/* first approach in list rendering
            <h2>{names[0]}</h2>
            <h2>{names[1]}</h2>
            <h2>{names[2]}</h2>
            <h2></h2> */}

            {/* second approach   
        {
            names.map(name => <h2>{name}</h2> )     
        } */}

            {/* third approach */}
            {/* {NameList} */}

            {/* last approach */}
            {/* {personList} */}


        </div>
    )
}

export default NameList

import React, { Component } from 'react'

class LifecycleB extends Component {
  
    constructor(props) {
      super(props)
    
      this.state = {
         name: 'Sweta'
      }
      console.log("LifecycleB constructor ");
    }

    static getDerivedStateFromProps(props, state) {
        console.log("LIfecycleB getDerivedFromProps");
        return null
    }
    componentDidMount(){
        console.log("LIfecycleB componentDIdMount");
    }

    shouldComponentUpdate() {
        console.log("LifecycleB shouldComponentUpdate");
        return true
      }
    
      getSnapshotBeforeUpdate(prevProps, prevState) {
        console.log("LifecycleB getSnapshotBeforeUpdate");
        return null
      }
    
      componentDidUpdate() {
        console.log("LifecycleB componentDidUpdate");
      }

    
    render() {
        console.log("LifecycleB render");
    return  <div>LIfecycleB</div>
  }
}


export default LifecycleB

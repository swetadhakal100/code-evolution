import React from 'react'

// function Greet (props) {
//     return <h1>Hello {props.name}</h1>
// }

//first way to pass props as a destructing
// const Greet = ({name, heroName}) => {
//     return (
//         <div>
//             <h1>Hello {name} a.k.a {heroName}</h1>
//             {/* {props.children} */}
//         </div>
//     )

//second way to pass props as destructing
const Greet = props => {
    const {name, heroName} = props
    return (
        <div>
            <h1>Hello {name} a.k.a {heroName}</h1>
        </div>
    )
}

export default Greet
import React, { useState } from "react";

const initState = {
  fname: "Bruce",
  lname: "Wayne",
};

const ObjectUseState = () => {
  const [person, setPerson] = useState(initState);
  const changeName = () => {
    // person.fname = "sweta";
    // person.lname = "dhakal";
    // setPerson(person);
    const newPerson = { ...person };
    newPerson.fname = "sweta";
    newPerson.lname = "dhakal";
    setPerson(newPerson);
  };
  console.log("ObjectUseState Rendering");
  return (
    <div>
      <button onClick={changeName}>
        {person.fname}
        {person.lname}
      </button>
    </div>
  );
};

export default ObjectUseState;

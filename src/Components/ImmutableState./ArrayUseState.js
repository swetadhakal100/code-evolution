import React, { useState } from "react";

const initState = ["sweta", "dhakal"];

const ArrayUseState = () => {
  const [persons, setPersons] = useState(initState);

  const handleClick = () => {
    //     persons.push("switu");
    //     persons.push("neha");
    //     setPersons(persons);

    const newPersons = [...persons];
    newPersons.push("switu");
    newPersons.push("nihu");
    setPersons(newPersons);
  };
  console.log("Array usestate rendering");

  return (
    <div>
      <button onClick={handleClick}>Click</button>
      {persons.map((person) => (
        <div key={person}>{person}</div>
      ))}
    </div>
  );
};

export default ArrayUseState;

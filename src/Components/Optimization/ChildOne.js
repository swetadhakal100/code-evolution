import React from "react";

const ChildOne = () => {
  console.log("ChildOne render");
  return <div>child component</div>;
};

export default ChildOne;

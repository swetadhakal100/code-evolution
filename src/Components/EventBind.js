import React, { Component } from 'react'

class EventBind extends Component {
    constructor(props) {
        super(props)

        this.state = {
            message: 'Hello'
        }

        this.clickHandler = this.clickHandler.bind(this)
    }

    clickHandler() {
        this.setState({
            message: 'Goodbye'
        })

    }
    //this is last approach
    // clickHandler = () =>{
    //     this.setState({
    //         message: 'Goodbye'
    //     })
    // }

    render() {
        //first way to bind this keyword..this keyword must be bind because this keyword will be undefined while fetching method in setstate.
        // return (
        //   <div>
        //     <div>{this.state.message}</div>
        //     <button onClick={this.clickHandler.bind(this)}>Click</button> 
        //   </div>
        // )

        //second way to bind this keyword in arrow function
        // return (
        //     <div>
        //         <div>{this.state.message}</div>
        //         <button onClick={() => this.clickHandler()}>Click</button>
        //     </div>
        // )

        //third way to bind this keyword
         return (
            <div>
                <div>{this.state.message}</div>
                <button onClick={this.clickHandler}>Click</button>
            </div>
        )
    }
}

export default EventBind

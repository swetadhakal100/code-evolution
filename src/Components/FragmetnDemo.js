import React from 'react'

function FragmetnDemo() {
    return (
        <React.Fragment>
            <h1>
                FragmetnDemo
            </h1>
            <p>This describe Fragment Demo component</p>

        </React.Fragment>

    )
}

export default FragmetnDemo

import React, { useState } from "react";
import useInput from "./hooks/useInput";

function UserForm() {
  const [firstName, bindFirstname, resetFirstName] = useInput("");
  const [lastName, bindLastname, resetLastName] = useInput("");

  const submitHandler = (e) => {
    e.preventDefault();
    alert(`Hello ${firstName} and ${lastName} is submitted`);
    resetFirstName();
    resetLastName();
  };

  return (
    <div>
      <form onSubmit={submitHandler}>
        <div>
          <label>firstName</label>
          <input {...bindFirstname} type="text" />
        </div>
        <div>
          <label>lastName</label>
          <input {...bindLastname} type="text" />
        </div>
        <button type="submit">Submit</button>
      </form>
    </div>
  );
}

export default UserForm;

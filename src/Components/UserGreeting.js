import React, { Component } from 'react'

class UserGreeting extends Component {
    constructor(props) {
        super(props)

        this.state = {
            isLoggedIn: false
        }
    }

    render() {
        //short circuit operator
        return this.state.isLoggedIn && <div>Welcome Sweta</div>


        //approach of element variables
        // let message
        // if(this.state.message){
        //     message = <div>Welcome Sweta</div>
        // }else{
        //     message = <div>Welcome Guest</div>
        // }
        // return <div>{message}</div>

        //approach of if/else case
        // if(this.state.isLoggedIn) {
        //     return(
        //         <div>
        //             Welcome Sweta
        //         </div>
        //     )
        // } else{
        //     return <div>Welcome Guest</div>
        // }

        //ternary operator
        // return (
        //     this.state.isLoggedIn ? <div>Welcome Sweta</div> : <div>Welcome Guest</div>
        // )
    }
}

export default UserGreeting

import React, { Component } from 'react'
import LifecycleB from './LifecycleB';

class LifecycleA extends Component {

  constructor(props) {
    super(props)

    this.state = {
      name: 'Sweta'
    }
    console.log("LifecycleA constructor ");
  }

  static getDerivedStateFromProps(props, state) {
    console.log("LIfecycleA getDerivedFromProps");
    return null
  }
  componentDidMount() {
    console.log("LIfecycleA componentDIdMount");
  }

  shouldComponentUpdate() {
    console.log("LifecycleA shouldComponentUpdate");
    return true
  }

  getSnapshotBeforeUpdate(prevProps, prevState) {
    console.log("LifecycleA getSnapshotBeforeUpdate");
    return null
  }

  componentDidUpdate() {
    console.log("LifecycleA componentDidUpdate");
  }

  changeState = () => {
    this.setState({
      name: 'Code-Evolution'
    })
  }


  render() {
    console.log("LifecycleA render");
    return (
      <div>


        <div>LIfecycleA</div>
        <button onClick={this.changeState}>Change State</button>
        <LifecycleB />

      </div>

    )
  }
}


export default LifecycleA

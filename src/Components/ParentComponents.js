import React, { useState, useCallback } from 'react'
import Title from './Title'
import Count from './Count'
import Button from './Button'


function ParentComponents() {
    const [age, setAge] = useState(25)
    const [salary, setSalary] = useState(50000)

    const incrementAge = useCallback(() => {
        setAge(age + 1)
    }, [age])
    const incrementSalary = useCallback(() => {
        setSalary(salary + 1000)
    }, [salary])
    return (
        <div>
            <Title />
            <Count text="Age" Count={age} />
            <Button handleClick={incrementAge}>Increment age</Button>
            <Count text="Salary" Count={salary} />
            <Button handleClick={incrementSalary}>Increment salary</Button>
        </div>
    )
}
export default ParentComponents

// import './App.css';
// import ClassClick from './Components/ClassClick';
// import Counter from './Components/Counter';
// import EventBind from './Components/EventBind';
// import FunctionClick from './Components/FunctionClick';
// import Greet from './Components/Greet';
// import Hello from './Components/Hello';
// import Inline from './Components/Inline';
// import Message from './Components/Message';
// import NameList from './Components/NameList';
// import ParentComponent from './Components/ParentComponent';
// import Stylesheet from './Components/Stylesheet';
// import UserGreeting from './Components/UserGreeting';
// import Welcome from './Components/Welcome';
// import './appStyles.css';
// import styles from './appStyles.module.css'
// import Form from './Components/Form';
// import LifecycleA from './Components/LifecycleA';
// import ParentComp from "./Components/ParentComp";
// import HookCounterFour from "./Components/HookCounterFour";
// import HookCounterThree from "./Components/HookCounterThree";
// import ClickCounter from "./Components/ClickCounter";
// import ClickCounterTwo from "./Components/ClickCounterTwo";
// import ComponentC from "./Components/ComponentC";
// import Counters from "./Components/Counters";
// import ErrorBoundary from "./Components/ErrorBoundary";
// import FRParentInput from "./Components/FRParentInput";
// import FocusInput from "./Components/FocusInput";
// import Hero from "./Components/Hero";
// import HoverCounter from "./Components/HoverCounter";
// import HoverCounterTwo from "./Components/HoverCounterTwo";
// import ClassCounter from "./Components/ClassCounter";
// import HookCounterTwo from "./Components/HookCounterTwo";
// import HookCounterTwo from "./Components/HookCounterTwo";
// import PostForm from "./Components/PostForm";
// import PostList from "./Components/PostList";
// import User from "./Components/User";
// import { UserProvider } from "./Components/userContext";
// import PortalDemo from "./Components/PortalDemo";
// import RefsDemo from "./Components/RefsDemo";
// import FragmetnDemo from "./Components/FragmetnDemo";
// import Table from "./Components/Table";
// import ClassCounterOne from "./Components/ClassCounterOne";
// import HookCounterOne from "./Components/HookCounterOne";
// import ClassMouse from "./Components/ClassMouse";
// import HookMouse from "./Components/HookMouse";
// import IntervalClassCounter from "./Components/IntervalClassCounter";
// import IntervalHookCounter from "./Components/IntervalHookCounter";
// import MouseContainer from "./Components/MouseContainer";
// import DataFetching from "./Components/DataFetching";
// import CounterOne from "./Components/CounterOne";

import { ChildA } from "./Components/Context/ContextChildren";
import ContextParent from "./Components/Context/ContextParent";
// import ParentFour from "./Components/IncorrectOptimization/ParentFour";
// import ParentThree from "./Components/IncorrectOptimization/ParentThree";
// import ChildOne from "./Components/Optimization/ChildOne";
// import GrandParent from "./Components/Optimization/GrandParent";
// import ParentOne from "./Components/Optimization/ParentOne";
// import ParentTwo from "./Components/Optimization/ParentTwo";

// import ArrayUseState from "./Components/ImmutableState./ArrayUseState";
// import ObjectUseState from "./Components/ImmutableState./ObjectUseState";

// import Parent from "./Components/ParentChild/Parent";

// import UseReducer from "./Components/UseState/UseReducer";
// import UseState from "./Components/UseState/UseState";

// import CounterOnes from "./Components/CounterOnes";
// import CounterTwos from "./Components/CounterTwos";
// import UserForm from "./Components/UserForm";

// import DocTitleTwo from "./Components/DocTitileTwo";
// import DocTitleOne from "./Components/DocTitleOne";
// import HookTimer from "./Components/HookTimer";

// import ClassTImer from "./Components/ClassTImer";

// import Counterss from "./Components/Counterss";
// import FocusInputs from "./Components/FocusInputs";

// import ParentComponents from "./Components/ParentComponents";

// import DataFetchingOne from "./Components/DataFetchingOne";
// import DataFetchingTwo from "./Components/DataFetchingTwo";

// import React, { useReducer } from "react"
// import ComponentssA from "./Components/ComponentssA"
// import ComponentssB from "./Components/ComponentssB"
// import ComponentssC from "./Components/ComponentssC"

// import CounterThree from "./Components/CounterThree";
// import CounterTwo from "./Components/CounterTwo";

// import React from "react";
// import ComponentsC from "./Components/ComponentsC";

// export const UserContext = React.createContext()
// export const ChannelContext = React.createContext()

// export const CountContext = React.createContext()

// const initialState = 0

// const reducer = (state, action) => {
//   switch (action) {
//     case 'increment':
//       return state + 1
//     case 'decrement':
//       return state - 1
//     case 'reset':
//       return initialState
//     default:
//       return state
//   }
// }

function App() {
  // const [count, dispatch] = useReducer(reducer, initialState)
  return (
    // <CountContext.Provider value = {{ countState: count, countDispatch: dispatch }}>
    <div className="App">
      <ContextParent>
        <ChildA />
      </ContextParent>

      {/* <ParentFour /> */}

      {/* <ParentThree /> */}

      {/* <ParentTwo /> */}

      {/* <GrandParent /> */}

      {/* <Parent /> */}

      {/* <ArrayUseState /> */}

      {/* <ObjectUseState /> */}

      {/* <UseReducer /> */}

      {/* <UseState /> */}

      {/* <UserForm /> */}

      {/* <CounterOnes /> */}
      {/* <CounterTwos /> */}

      {/* <DocTitleTwo /> */}

      {/* <DocTitleOne /> */}

      {/* <HookTimer/> */}

      {/* <ClassTImer/> */}

      {/* <FocusInputs/> */}

      {/* <Counterss/> */}

      {/* <ParentComponents/> */}

      {/* <DataFetchingTwo/> */}

      {/* <DataFetchingOne /> */}

      {/* Count - {count} */}
      {/* <ComponentssA />
        <ComponentssB />
        <ComponentssC /> */}

      {/* <CounterThree /> */}

      {/* <CounterTwo/> */}

      {/* <CounterOne/> */}

      {/* <UserContext.Provider value={'Sweta Dhakal'}>
      <ChannelContext.Provider value={'Code Evolution'}>
      <ComponentsC/>
      </ChannelContext.Provider>
    </UserContext.Provider> */}

      {/* <DataFetching/> */}

      {/* <IntervalHookCounter/> */}

      {/* <IntervalClassCounter/> */}

      {/* <MouseContainer/> */}

      {/* <HookMouse/> */}

      {/* <ClassMouse/> */}

      {/* <HookCounterOne/> */}

      {/* <ClassCounterOne/> */}

      {/* <HookCounterFour/> */}

      {/* <HookCounterThree/> */}

      {/* <HookCounterTwo/> */}

      {/* <HookCounter/> */}

      {/* <ClassCounter/> */}

      {/* <PostForm/> */}

      {/* <PostList/> */}

      {/* <UserProvider value="sweta">
        <ComponentC/>
      </UserProvider> */}

      {/* <Counters render={(count, incrementCount)=> <ClickCounterTwo count={count} incrementCount={incrementCount}></ClickCounterTwo> }/>

      <Counters render={(count, incrementCount)=> <HoverCounterTwo count={count} incrementCount={incrementCount}></HoverCounterTwo> }/> */}

      {/* <ClickCounterTwo/>
      <HoverCounterTwo/>
      <User render= { (isLoggedIn) => isLoggedIn ? "sweta" : "Guest"}/> */}

      {/* <ClickCounter name="sweta" /> */}
      {/* 
      <HoverCounter /> */}

      {/* <ErrorBoundary>
        <Hero heroName="Batman" />
      </ErrorBoundary>


      <ErrorBoundary>
        <Hero heroName="Superman" />
      </ErrorBoundary>


      <ErrorBoundary>
        <Hero heroName="Joker" />
      </ErrorBoundary> */}

      {/* <PortalDemo/> */}

      {/* <FRParentInput/> */}

      {/* <FocusInput/> */}

      {/* <RefsDemo/> */}

      {/* <ParentComp/> */}

      {/* <Table/> */}

      {/* <FragmetnDemo/> */}

      {/* <LifecycleA /> */}

      {/* <Form/> */}
      {/* <h1 className='error'>Error</h1>
      <h1 className={styles.success}>Success</h1>
      <Inline/>  */}
      {/* <Stylesheet primary={true}/> */}
      {/* <NameList/> */}
      {/* <UserGreeting/> */}
      {/* <ParentComponent/> */}
      {/* <EventBind/> */}
      {/* <ClassClick/> */}
      {/* <FunctionClick/> */}
      {/* <Counter/> */}
      {/* <Message/> */}
      {/* <Hello/> */}
      {/* <Welcome name='Sweta' heroName="Batman"/>
      <Welcome name='Sweta' heroName="Batman"/> */}
      {/* <Welcome name='Sweta' heroName="Batman"/>
      <Greet name='Sweta' heroName="Batman"> <p>This is children props</p></Greet> */}
      {/* <Greet name='Neha' heroName="SuperName"><button>Action</button></Greet>
      <Greet name='Sonu' heroName="WonderWomen" /> */}
    </div>
    // </CountContext.Provider>
  );
}

export default App;
